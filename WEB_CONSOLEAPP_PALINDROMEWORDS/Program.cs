﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WEB_CONSOLEAPP_PALINDROMEWORDS
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a word: ");
            var result = CheckWord(Console.ReadLine());

            Console.WriteLine(result);
            Console.ReadKey();
        }

        static bool CheckWord(string Word)
        {
            var PalindromeWord = Word.Replace(" ", "");
            var Length = PalindromeWord.Length-1;

            for (int i = 0; i <= PalindromeWord.Length -1; i++)
            {
                if(!PalindromeWord.Substring(i,1).Equals(PalindromeWord.Substring((Length-i),1), StringComparison.InvariantCultureIgnoreCase))
                {
                    return false;
                }
            }
            
            return true;
        }
    }
}
j